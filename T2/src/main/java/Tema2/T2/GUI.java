package Tema2.T2;
import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

public class GUI extends JFrame{
	JLabel l1=new JLabel("Number of queues: ");
	JLabel l4=new JLabel("  Simulation interval(s): ");
	JLabel l2=new JLabel("Service time (MIN,MAX): ");
	JLabel l3=new JLabel("Spawn time (MIN,MAX): ");
	JTextField tf1=new JTextField(5);
	JTextField tf2=new JTextField(5);
	JTextField tf3=new JTextField(5);
	JTextField tf4=new JTextField(5);
	JTextField tf5=new JTextField(5);
	JTextField tf6=new JTextField(5);
	JButton b1=new JButton("OK");
	public GUI(){
		JPanel p1=new JPanel();
		p1.add(l1);
		p1.add(tf1);
		p1.add(l4);
		p1.add(tf6);
		p1.add(l2);
		p1.add(tf2);
		p1.add(tf3);
		p1.add(l3);
		p1.add(tf4);
		p1.add(tf5);
		p1.add(b1);
		p1.setLayout(new FlowLayout());
		this.setContentPane(p1);
		this.pack();
		this.setTitle("Informations");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	String getNr() {
		return tf1.getText();
		}
	String getSimulationTime() {
		return tf6.getText();
		}
	String getServiceTimeMIN() {
		return tf2.getText();
		}
	String getServiceTimeMAX() {
		return tf3.getText();
		}
	String getSpawnTimeMIN() {
		return tf4.getText();
		}
	String getSpawnTimeMAX() {
		return tf5.getText();
		}
	void addOKListener1(ActionListener k) {
		b1.addActionListener(k);
		}
}
