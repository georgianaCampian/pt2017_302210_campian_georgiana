package Tema2.T2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class Control {
	private GUI v;
	private static int nr,simulationT;
	private static int serviceMIN,serviceMAX;
	private static int spawnMIN,spawnMAX;
	public Control(GUI view) {
		v = view;
		view.addOKListener1(new OKListener1());
	}
	class OKListener1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			nr=Integer.parseInt(v.getNr());
			serviceMIN=Integer.parseInt(v.getServiceTimeMIN());
			serviceMAX=Integer.parseInt(v.getServiceTimeMAX());
			spawnMIN=Integer.parseInt(v.getSpawnTimeMIN());
			spawnMAX=Integer.parseInt(v.getSpawnTimeMAX());
			simulationT=Integer.parseInt(v.getSimulationTime());
			Shop s=new Shop(nr,serviceMIN,serviceMAX,spawnMIN,spawnMAX,simulationT);
			Thread t=new Thread(s);
			t.start();
			if (s.getFinish()==true){
				t.interrupt();
			//	t.stop();
			}
		}
	}		
}
