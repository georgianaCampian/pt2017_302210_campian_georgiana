package Tema2.T2;

import java.util.LinkedList;
import java.util.Random;
public class ClientGenerator extends Thread{
	private int serviceMIN;
	private int serviceMAX;
	private int arrivalTime;
	private int spawnTime;
	private int spawnMIN;
	private int spawnMAX;
	private int i=0;
	private LinkedList <Client> cList=new LinkedList<Client>();
	private Client client;
	public ClientGenerator(int serviceMIN,int serviceMAX,int spawnMIN,int spawnMAX){
		this.serviceMIN=serviceMIN;
		this.serviceMAX=serviceMAX;
		this.spawnMAX=spawnMAX;
		this.spawnMIN=spawnMIN;
	}

	synchronized public void run(){
		while (true) {
			if (Shop.stopG==false){
				i++;
				Random random = new Random();
				int t =random.nextInt(serviceMAX - serviceMIN) + serviceMIN;
				spawnTime = spawnMIN+ random.nextInt(spawnMAX - spawnMIN);
				arrivalTime=Shop.getCurrentTime();
				client = new Client(t,i-1,arrivalTime);
				cList.add(client);
				try {
					Thread.sleep(spawnTime*1000);
				} catch (InterruptedException e) {
					break;
				}
			}
			else break;
		}
	}
	public LinkedList <Client> getClientList(){
		return cList;
	}
	public Client getNewClient(){
		return client;
	}
}
