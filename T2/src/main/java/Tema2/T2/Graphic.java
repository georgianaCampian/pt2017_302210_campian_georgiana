package Tema2.T2;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.*;

public class Graphic extends JComponent {
	private int n;
	private int width = 800;
	private ArrayList<ShopQueue> queueList;
	public Graphic(int nr, ArrayList<ShopQueue> queue) {
		n = nr;
		queueList = queue;
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawCashRegisters(g);
	}

	public void drawCashRegisters(Graphics g) {
		int i;
		for (i = 0; i < n; i++) {
			int y=100;
			g.setColor(Color.black);
			g.fillRect(width / n + i * 100, 100, 80, 40);
			g.setColor(Color.WHITE);
			g.drawString("Cashier nr." + i, width / n + i * 100, 110);
			checkQueue(i,g,y);
			this.repaint();
		}
	}

	public void checkQueue(int i,Graphics g,int y) {
		int j;
		for (j=0; j<queueList.get(i).getListSize(); j++){
			int id=queueList.get(i).getCurrentClient(j).getID();
			y+=45;
			if (queueList.get(i).getCurrentClient(j).getServiceTime()<6) {
				g.setColor(Color.green);
			}
			else if (queueList.get(i).getCurrentClient(j).getServiceTime()<10){
				g.setColor(Color.orange);
			}
			else g.setColor(Color.red);
			drawClient(i,y,id,g);
		}
	}

	public void drawClient(int col, int row,int id, Graphics g) { // col=cashier nb starting from 0 // row=client nb in a queue starting from 1
		g.fillOval(width / n + col * 100 + 20, row, 40, 40);
		g.setColor(Color.black);
		g.drawString("ID:"+id, width / n + col * 100 + 27, row+25);
		this.repaint();
	}
	public void update(){
		this.repaint();
	}
}

