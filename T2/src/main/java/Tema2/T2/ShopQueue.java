package Tema2.T2;

import java.util.LinkedList;

public class ShopQueue extends Thread { // e o coada in sine
	private int emptyTime;
	private int waitingTime;
	private LinkedList<Client> clientList = new LinkedList<Client>();

	public ShopQueue() {
		emptyTime = 0;
		waitingTime = 0;
	}

	synchronized public void run() {
		while (true) {
			if (Shop.stopQ==false) {
				if (!clientList.isEmpty()) {
					try {
						Thread.sleep(clientList.getFirst().getServiceTime() * 1000);
						// System.out.println("---client removed:
						// "+clientList.getFirst().toString());
						Shop.setLog("Time:"+Shop.getCurrentTime()+" " + clientList.getFirst().toString() + " left \n");
						addWaitingTime(Shop.getCurrentTime() - clientList.getFirst().getArrivalTime()
								- clientList.getFirst().getServiceTime());
						clientList.removeFirst();
					} catch (InterruptedException e) {
						break;
					} 
				} else {
					emptyTime++;
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				}
			}
			else break;
		}
	}

	public void addClient(Client c) {
		clientList.add(c);
	}

	public Client removeClient() {
		return clientList.removeFirst();
	}

	public int getListSize() {
		return clientList.size();
	}

	public double getEmptyTime() {
		return emptyTime;
	}

	public void addWaitingTime(int t) {
		waitingTime += t;
	}

	public int getWaitingTime() {
		return waitingTime;
	}

	public boolean isEmpty() {
		return clientList.isEmpty();
	}

	public LinkedList<Client> getClientList() {
		return clientList;
	}

	public Client getCurrentClient(int i) {
		return clientList.get(i);
	}
}
