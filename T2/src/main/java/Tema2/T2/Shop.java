package Tema2.T2;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Shop extends JFrame implements Runnable {
	public static boolean stopG=false,stopQ=false;
	private static String log = "";
	private static boolean finish=false;
	private static int currentTime = 0;
	private int maxClients=0;
	private int nbOfClients=0;
	private int maxPeakSecond=0;
	private int nbOfQ;
	private int serviceMIN;
	private int serviceMAX;
	private int simulationT; 
	private int spawnMIN;
	private int spawnMAX;
	private double medWaitingTime;
	private double medEmptyTime;
	private double medServiceTime;
	private int totalServiceTime=0;
	private ArrayList<ShopQueue> queueList;
	private LinkedList<Client> clientList = new LinkedList<Client>();
	private Graphic graphics;
	public Shop(int nbOfQ, int serviceMIN, int serviceMAX, int spawnMIN, int spawnMAX, int simulationT) {
		int i;
		this.nbOfQ = nbOfQ;
		this.serviceMIN = serviceMIN;
		this.serviceMAX = serviceMAX;
		this.spawnMIN = spawnMIN;
		this.spawnMAX = spawnMAX;
		this.simulationT = simulationT ;
		queueList = new ArrayList<ShopQueue>(nbOfQ);
		for (i = 0; i < nbOfQ; i++) {
			queueList.add(new ShopQueue());
		}
	}
	synchronized public void run() {
		initFrame();
		ClientGenerator g = new ClientGenerator(serviceMIN, serviceMAX, spawnMIN, spawnMAX);
		int i,b;
		g.start();
		for (i = 0; i < nbOfQ; i++) {
			queueList.get(i).start();
		}
		while (currentTime < simulationT) {
			clientList=g.getClientList();
			if (!clientList.isEmpty()){
				for(i=0; i<clientList.size(); i++){
					if (!clientList.get(i).getAddedToList()){
						Client client=clientList.get(i);  //iau fiecare client din lista totala de clienti si incerc sa gasesc cea mai buna coada unde sa il pun
						b=getTheBestQueue();
						client.setAddedToList();
						queueList.get(b).addClient(client);
						nbOfClients++;
						setLog("Time:"+currentTime+" At queue nb. "+b+" "+client.toString()+" arrived \n");
						System.out.println("At queue "+b+" client: "+client);
						queueList.get(b).addWaitingTime(client.getServiceTime());
						totalServiceTime+=client.getServiceTime();
					}
				}
			}
			updateGraphic();
			setPeakSecond();
			currentTime++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		stopG=true;
		stopQ=true;
		g.interrupt();
		//g.stop();
    	System.out.println("g is interrupted? "+g.isInterrupted());
		for (i=0; i<nbOfQ; i++){
			queueList.get(i).interrupt();
		//	queueList.get(i).stop();
	//		System.out.println("interrupted? "+queueList.get(i).isInterrupted());
		}
		setFinish();
		setMedTimes();
		createLog();
	}
	public void initFrame(){
		JFrame frame = new JFrame();
	    graphics=new Graphic(nbOfQ,queueList);
		frame.add(graphics);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setTitle("Simulation");
		frame.setSize(800,600);
		setLog("simulation started\n");
	}
	public void setFinish(){
		finish=true;
	}
	public  boolean getFinish(){
		return finish;
	}
	synchronized public static String getLog() {
		return log;
	}
	synchronized public static void setLog(String s){
		log += s;
	}
	public void setMedTimes() {
		int i;
		double e = 0, w = 0;
		for (i = 0; i < nbOfQ; i++) {
			e = e + queueList.get(i).getEmptyTime();
			w = w + queueList.get(i).getWaitingTime();
		}
		medWaitingTime =(double) w / nbOfClients;
		medEmptyTime = (double) e / nbOfClients;
		medServiceTime= (double) totalServiceTime/nbOfClients;
		String s=new String("Average waiting time: "+String.format("%.2f", medWaitingTime)+"\nAverage serving time: "+String.format("%.2f",medServiceTime)+"\nAverage empty time: "+String.format("%.2f", medEmptyTime)+"\nPeak Second: "+maxPeakSecond);
		JOptionPane.showMessageDialog(null, s);
		setLog("\n "+s);
	}
	private void setPeakSecond() {
		int n = 0;
		for (int i = 0; i < queueList.size(); i++) {
			n += queueList.get(i).getListSize();
		}
		if (n > maxClients) {
			maxClients = n;
			maxPeakSecond = currentTime;
		}
	}
	public int getTheBestQueue() {
		int i;
		int min = Integer.MAX_VALUE;
		int qNb = 0;
		for (i = 0; i < nbOfQ; i++) {
			if (queueList.get(i).getWaitingTime() < min) {
				min = queueList.get(i).getWaitingTime();
				qNb = i;
			}
		}
		return qNb;
	}
	public void createLog(){
		FileWriter fileWriter = null;
		try {
            File newTextFile = new File("log.txt");
            fileWriter = new FileWriter(newTextFile);
            fileWriter.write(getLog());
            JOptionPane.showMessageDialog(null, "Log Created");
            fileWriter.close();
        } catch (IOException e) {
        	e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}
	public void updateGraphic(){
		graphics.update();
	}
	public static int getCurrentTime(){
		return currentTime;
	}
}
