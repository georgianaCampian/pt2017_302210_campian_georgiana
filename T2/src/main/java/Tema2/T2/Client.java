package Tema2.T2;

public class Client {
	private int serviceTime;
	private int arrivalTime;
	private int id;
	private boolean addedToList=false;
	private boolean addedArrivalTime=false;
	private boolean addedServiceTime=false;
	public Client(int serviceTime,int id,int arrivalTime){
		this.serviceTime=serviceTime;
		this.id=id;
		this.arrivalTime=arrivalTime;
	}
	public int getServiceTime(){
		return serviceTime;
	}
	public int getID(){
		return id;
	}
	public String toString(){
		return "Client with id "+id;
	}
	
	public void setAddedToList(){
		addedToList=true;
	}
	public boolean getAddedToList(){
		return addedToList;
	}
	public int getArrivalTime(){
		return arrivalTime;
	}
	public void addedArrivalTime(boolean b){
		addedArrivalTime=b;
	}
	public boolean getAddedArrivalTime(){
		return addedArrivalTime;
	}
	public void addedServiceTime(boolean b){
		addedServiceTime=b;
	}
	public boolean getAddedServiceTime(){
		return addedServiceTime;
	}
}
