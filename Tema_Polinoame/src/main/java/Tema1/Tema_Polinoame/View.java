package Tema1.Tema_Polinoame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View extends JFrame{
	JTextArea rezAdd=new JTextArea(1,20),rezS=new JTextArea(1,20),rezInm=new JTextArea(1,20);
	JPanel p1=new JPanel();
	JLabel l1=new JLabel("Polinomul 1:");
	JTextField tf1=new JTextField(20);
	JButton b1=new JButton("OK");
	JLabel l2=new JLabel("Polinomul 2:");
	JTextField tf2=new JTextField(20);
	JButton b2=new JButton("OK");
	JButton aduna=new JButton("Adunare ");
	JButton scade=new JButton("Scadere ");
	JButton inm=new JButton("Inmultire ");
	JButton inte=new JButton("Intergrare");
	JButton der=new JButton("  Derivare ");
	JLabel a=new JLabel("P1+P2:");
	JLabel s=new JLabel("P1-P2:");
	JLabel in=new JLabel("P1*P2:");
	JLabel i1=new JLabel("S P1:");
	JLabel i2=new JLabel("S P2:");
	JTextArea rezIn1= new JTextArea(1,20);
	JTextArea rezIn2= new JTextArea(1,20);
	JLabel d1=new JLabel("P1`:");
	JTextArea rezD1= new JTextArea(1,20);
	JLabel d2=new JLabel("P2`:");
	JTextArea rezD2= new JTextArea(1,20);
	public View(){
		JPanel p1=new JPanel();
		p1.add(l1,BorderLayout.NORTH);
		p1.add(tf1,BorderLayout.NORTH);
		p1.add(b1,BorderLayout.NORTH);
		p1.add(l2,BorderLayout.NORTH);
		p1.add(tf2,BorderLayout.NORTH);
		p1.add(b2,BorderLayout.NORTH);
		p1.setPreferredSize(new Dimension(300,200));
		JPanel p2=new JPanel();
		p2.add(aduna,BorderLayout.CENTER);
		p2.add( Box.createRigidArea(new Dimension(400,0)) );
		p2.add(scade);
		p2.add( Box.createRigidArea(new Dimension(400,0)) );
		p2.add(inm);
		p2.add( Box.createRigidArea(new Dimension(400,0)) );
		p2.add(inte);
		p2.add( Box.createRigidArea(new Dimension(400,0)) );
		p2.add(der);
		p2.setPreferredSize(new Dimension(300,200));
		//rezultate
		JPanel p3=new JPanel();
		p3.add(a);
		rezAdd.setEditable(false);
		p3.add(rezAdd);
		p3.add( Box.createRigidArea(new Dimension(24,0)) );
		p3.add(s);
		rezS.setEditable(false);
		p3.add(rezS);
		p3.add( Box.createRigidArea(new Dimension(24,0)) );
		p3.add(in);
		rezInm.setEditable(false);
		p3.add(rezInm);
		p3.add( Box.createRigidArea(new Dimension(24,0)) );
		p3.add(i1);
		rezIn1.setEditable(false);
		p3.add(rezIn1);
		p3.add( Box.createRigidArea(new Dimension(24,0)) );
		p3.add(i2);
		rezIn2.setEditable(false);
		p3.add(rezIn2);
		p3.add( Box.createRigidArea(new Dimension(26,0)) );
		p3.add(d1);
		rezD1.setEditable(false);
		p3.add(rezD1);
		p3.add( Box.createRigidArea(new Dimension(22,0)) );
		p3.add(d2);
		rezD2.setEditable(false);
		p3.add(rezD2);
		p3.add( Box.createRigidArea(new Dimension(22,0)) );
		p3.setPreferredSize(new Dimension(250,400));
		p1.setBackground(Color.PINK);
		p2.setBackground(Color.PINK);
		p3.setBackground(Color.PINK);
		JPanel p=new JPanel();
		p.add(p1);
		p.add(p2);
		p.add(p3);
		p.setLayout(new FlowLayout());
		p.setBackground(Color.PINK);
	this.setContentPane(p);
	this.pack();
	this.setTitle("Polinoame");
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	//metode pentru preluarea/scrierea datelor
	String getPol1() {
		return tf1.getText();
		}
	String getPol2() {
		return tf2.getText();
		}
	void setAdunare(String pAdd) {
		rezAdd.setText(pAdd);
		}
	void setScadere(String pS) {
		rezS.setText(pS);
		}
	void setInm(String pI) {
		rezInm.setText(pI);
		}
	void setIntegrat1(String pI) {
		rezIn1.setText(pI);
		}
	void setIntegrat2(String pI) {
		rezIn2.setText(pI);
		}
	void setDerivat1(String pD) {
		rezD1.setText(pD);
		}
	void setDerivat2(String pD) {
		rezD2.setText(pD);
		}
	//ascultatori
	void addOKListener1(ActionListener k) {
		b1.addActionListener(k);
		}
	void addOKListener2(ActionListener k) {
		b2.addActionListener(k);
		}
	void addAdListener(ActionListener k) {
		aduna.addActionListener(k);
		}
	void addSListener(ActionListener k) {
		scade.addActionListener(k);
		}
	void addInmListener(ActionListener k) {
		inm.addActionListener(k);
		}
	void addIntListener(ActionListener k) {
		inte.addActionListener(k);
		}
	void addDerListener(ActionListener k) {
		der.addActionListener(k);
		}
}
