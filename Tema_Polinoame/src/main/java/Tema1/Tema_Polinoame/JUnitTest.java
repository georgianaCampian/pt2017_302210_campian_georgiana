package Tema1.Tema_Polinoame;
import static org.junit.Assert.*;
import org.junit.*;
public class JUnitTest {
	String m1[]={"6x^2","4x","5"};
	String s1[]={"+","+","+","+","+"};
	String m2[]={"5x","3"};
	String s2[]={"+","-","+","+","+"};
	Polinom p1=new Polinom(m1,s1);
	Polinom p2=new Polinom(m2,s2);
	@Test
	public void testAdunare() {
		String s=p1.adunaPolinom(p2).print();
		assertEquals("+6x^2+9x^1+2",s);
	}	
	@Test
	public void testScadere() {
		String s=p1.minusPolinom(p2).adunaPolinom(p1).print();
		assertEquals("+6x^2-1x^1+8",s);
	}	
	@Test
	public void testInmultire() {
		String s=p1.inmultirePolinom(p2).print();
		assertEquals("+30x^3+2x^2+13x^1-15",s);
	}	
	@Test
	public void testIntegrare() {
		String s1=p1.integrarePolinom().print();
		String s2=p2.integrarePolinom().print();
		assertEquals("+2,00x^3+2,00x^2+5,00x^1",s1);
		assertEquals("+2,50x^2-3,00x^1",s2);
	}	
	@Test
	public void testDerivare() {
		String s1=p1.derivarePolinom().print();
		String s2=p2.derivarePolinom().print();
		assertEquals("+12x^1+4 ",s1);
		assertEquals("+5 ",s2);
	}	
}
