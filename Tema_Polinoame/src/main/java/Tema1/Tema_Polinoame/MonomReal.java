package Tema1.Tema_Polinoame;
public class MonomReal {
	private double co;
	private int po;
	private boolean ad;
	public MonomReal(double c,int p,boolean a){
		co=c;
		po=p;
		ad=a;
	}
	public void setCoefR(double c){
		co=c;
	}
	public void setPowerR(int p){
		po=p;
	}
	public String toString(){    
		if (co==0){
			return " ";
		}
		else {  if (co<0) {
						if (po==0) return String.format("%.2f",co)+"";
						else return String.format("%.2f",co)+"x"+"^"+po;}
				else {
					if (po==0) return "+"+ String.format("%.2f",co);
					else return "+"+ String.format("%.2f",co)+"x"+"^"+po;}
		}
	}
	
}
