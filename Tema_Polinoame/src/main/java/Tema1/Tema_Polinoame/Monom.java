package Tema1.Tema_Polinoame;

public class Monom {
	private int coef,power;
	private boolean ad;
	public Monom(int c,int p,boolean a){
		coef=c;
		power=p;
		ad=a;
	}
	public Monom adunaMonom(Monom m){
		Monom new_monom=new Monom(0,0,true);
		int new_coef=0;
			new_coef=coef+m.coef;
			new_monom.setNewCoef(new_coef);
			new_monom.setNewPower(power);
		return new_monom;
	}
	public Monom inmultesteMonom(Monom m) {
		Monom new_monom=new Monom(0,0,true);
		new_monom.setNewCoef(coef*m.coef);
		new_monom.setNewPower(power+m.power);
		return new_monom;
	}
	public void setNewCoef(int c){
		coef=c;
	}
	public String toString(){
		if (coef==0){
			return " ";
		}
		else {  if (coef<0) {
						if (power==0) return coef+"";
						else return coef+"x"+"^"+power;}
				else {
					if (power==0) return "+"+coef;
					else return "+"+coef+"x"+"^"+power;}
		}
	}
	public int getCoef(){
		return coef;
	}
	public int getPower(){
		return power;
	}
	public void setNewPower(int p){
		power=p;
	}
	public void adaugat(boolean b){
		ad=b;
	}
	public boolean getAdaugat(){
		return ad;
	}
}
