package Tema1.Tema_Polinoame;
import java.util.Comparator;

public class ComparePower implements Comparator <Monom> {

	public int compare(Monom m, Monom m1) {
		return m1.getPower()-m.getPower();
	}

}
