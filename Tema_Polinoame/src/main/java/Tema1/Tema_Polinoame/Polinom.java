package Tema1.Tema_Polinoame;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
public class Polinom {
	int co,po,k=0,lng,i;
	public ArrayList<Monom> monom = new ArrayList<Monom>( );
	public Polinom(String[] myStrings,String[] semne){
		boolean ok=false;
		int j=0,d;
		if (semne[0].equals("-")) ok=true;
		lng=myStrings.length;
		if (ok==true) j=1;
		for(i=j; i<lng; i++){
			// verificam si adaugam in lista fiecare monom dintr-un polinom
		   if (ok==true) d=i-1;
		   else d=i;
		   if (isNumeric(myStrings[i]))  
		   {
			    co = Integer.parseInt(myStrings[i]);   //t va fi coef lui x^0
			    if (semne[d].equals("+")){
			   monom.add(new Monom(co,0,false));  //puterea va fi 0
			    }
			    else  monom.add(new Monom((-1)*co,0,false));  //puterea va fi 0
		   }
		   else  //contine x si trebuie luate doar coef si puterea cu split
		   {   
		   		//verificam daca e de tipul ax => puterea=1
			    if ((myStrings[i].indexOf("^"))==-1) 
			    {
			    	po=1;
			    	//e de tipul ax si verificam daca a=1
			    	if (myStrings[i].length()==1)
			    	{
			    		// monomul va fi x =>coef=1
			    		co=1;
			    	}
			    	else {
			    		String x[]=myStrings[i].split("x"); 
			    		co=Integer.parseInt(x[0]);
			    	}
			    }
			    else // e de tipul ax^b
			    {
			    	//verificam daca e de tipul x^b =>coef=1
			    	if (myStrings[i].length()==3){
			    		co=1;
			    		String p[]=myStrings[i].split("\\^|x");
			    		po=Integer.parseInt(p[2]);
			    	}
			    	else {  //e de tipul ax^b
			    		String s[]=myStrings[i].split("\\^|x");
			    		co=Integer.parseInt(s[0]);
			    		po=Integer.parseInt(s[2]);
			    	}
			    }
			    if (semne[d].equals("+")){
			    	monom.add(new Monom(co,po,false));
			    }
			    else monom.add(new Monom((-1)*co,po,false));
		   }
		  
		}
	}
	public boolean isNumeric(String str)
	{
	  return str.matches("-?\\d+(\\.\\d+)?");  
	}
	
	public Polinom adunaPolinom(Polinom p){
		String s[]={"+","+","+","+","+"};
		String[] r={};
		Polinom sumaP=new Polinom(r,s);
		int i,j,n=monom.size(),m=p.monom.size();
		for (i=0; i<n; i++){   //initial niciun monom nu e adaugat in polinomul suma
			monom.get(i).adaugat(false);
		}
		for (j=0; j<m; j++){
			p.monom.get(j).adaugat(false);
		}
		//facem adunarea monoamelor, daca l-am putut aduna atunci adaugat va fi true, pt ca la final cand nu am putut aduna anumiti termeni care
		//nu aveau aceleasi puteri sa ii adaugam si pe ei
		for (i=0; i<n; i++){
			for(j=0; j<m; j++){
				if (monom.get(i).getPower()==p.monom.get(j).getPower()){
				    sumaP.setMonom(monom.get(i).adunaMonom(p.monom.get(j)));  
				    monom.get(i).adaugat(true);
				    p.monom.get(j).adaugat(true);
				}
			}
		}
		//adaugam in polinomul suma monoamele ce au ramas neadunate din ambele polinoame
		for (i=0; i<n; i++){
			if (monom.get(i).getAdaugat()==false){
				sumaP.setMonom(monom.get(i));
			}
		}
		for (j=0; j<m; j++){
			if (p.monom.get(j).getAdaugat()==false){
				sumaP.setMonom(p.monom.get(j));
			}
		}
		Comparator <Monom> comp=new ComparePower();
		Collections.sort(sumaP.monom,comp);
		return sumaP;
	}
	
	public Polinom minusPolinom(Polinom p){
		String s[]={"+","+","+","+","+"};
		String[] r={};
		Polinom negPol=new Polinom(r,s);
		int n=p.monom.size();  //cate monoame are lista
		int i,coef,po;
		for (i=0; i<n; i++){
			coef=p.monom.get(i).getCoef();
			po=p.monom.get(i).getPower();
			coef=coef*(-1);
			negPol.setMonom(new Monom(coef,po,false));  //facem un nou polinom (-1)*cel scazut, pe care il vom aduna la primul
		}	
		return negPol;
	}
	public PolinomReal integrarePolinom(){
		String s[]={"+","+","+","+","+"};
		String[] r={};
		PolinomReal intPol=new PolinomReal(r,s);
		int n=monom.size(),c,p; 
		int j;
		for (j=0; j<n ;j++){
			c=monom.get(j).getCoef();
			p=monom.get(j).getPower();
			MonomReal m=new MonomReal((double)(c)/(p+1),p+1,false);
			intPol.setMonomR(m);
		}
		return intPol;
	}
	public Polinom derivarePolinom(){
		String s[]={"+","+","+","+","+"};
		String[] r={};
		Polinom derPol=new Polinom(r,s);
		int n=monom.size(),c,p; 
		int j;
		for (j=0; j<n; j++){
			c=monom.get(j).getCoef();
			p=monom.get(j).getPower();
			Monom m=new Monom(c*p,p-1,false);
			derPol.setMonom(m);
		}
		return derPol;
	}
	public void setMonom(Monom m){
		monom.add(m);
	}
	public String print(){
		String s="";
		for (Monom each:monom){
			s=s+each.toString();
		}
		return s;
	}
	public Polinom inmultirePolinom(Polinom p) {
		String s[]={"+","+","+","+","+"};
		String[] r={};
		Polinom iPol=new Polinom(r,s);
		Polinom finalPol=new Polinom(r,s);
		int i,j,n=monom.size(),m=p.monom.size();
		for (i=0; i<n; i++){
			for (j=0; j<m; j++){
				Monom mo=monom.get(i).inmultesteMonom(p.monom.get(j));
				iPol.setMonom(mo);
			}
		}
		//iPol o sa fie de genul 2x+4x+1+3 si vrem sa grupam termenii la fel
		n=iPol.monom.size();
		for (j=0; j<n; j++){
			iPol.monom.get(j).adaugat(false);
		}
		for (i=0; i<n; i++){
			for (j=i+1; j<n; j++){
				if (iPol.monom.get(i).getPower()==iPol.monom.get(j).getPower()){
					Monom mo=iPol.monom.get(i).adunaMonom(iPol.monom.get(j));
					iPol.monom.get(i).adaugat(true);
					iPol.monom.get(j).adaugat(true);
					finalPol.setMonom(mo);
				}
			}
		}
		for(i=0; i<n; i++){
			if (iPol.monom.get(i).getAdaugat()==false){
			
				finalPol.setMonom(iPol.monom.get(i));
				}
		}
		Comparator <Monom> comp=new ComparePower();
		Collections.sort(finalPol.monom,comp);
		return finalPol;
	}
}
