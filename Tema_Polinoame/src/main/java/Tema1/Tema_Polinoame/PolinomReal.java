package Tema1.Tema_Polinoame;
import java.util.ArrayList;

public class PolinomReal {
	double co;
	int k=0,lng,i,po;
	public ArrayList<MonomReal> monomR = new ArrayList<MonomReal>( );
	public PolinomReal(String[] myStrings,String[] semne){
		boolean ok=false;
		int j=0,d;
		if (semne[0].equals("-")) ok=true;
		lng=myStrings.length;
		if (ok==true) j=1;
		for(i=j; i<lng; i++){
		   if (ok==true) d=i-1;
		   else d=i;
		   if (isNumeric(myStrings[i]))  
		   {
			    co = Double.parseDouble(myStrings[i]);   //t va fi coef lui x^0
			    if (semne[d].equals("+")){
			   monomR.add(new MonomReal(co,0,false));  //puterea va fi 0
			    }
			    else  monomR.add(new MonomReal((-1)*co,0,false));  //puterea va fi 0
		   }
		   else  //contine x si trebuie luate doar coef si puterea cu split
		   {   
		   		//verificam daca e de tipul ax => puterea=1
			    if ((myStrings[i].indexOf("^"))==-1) 
			    {
			    	po=1;
			    	//e de tipul ax si verificam daca a=1
			    	if (myStrings[i].length()==1)
			    	{
			    		// monomul va fi x =>coef=1
			    		co=1;
			    	}
			    	else {
			    		String x[]=myStrings[i].split("x"); 
			    		co=Double.parseDouble(x[0]);
			    	}
			    }
			    else // e de tipul ax^b
			    {
			    	//verificam daca e de tipul x^b =>coef=1
			    	if (myStrings[i].length()==3){
			    		co=1;
			    		String p[]=myStrings[i].split("\\^|x");
			    		po=Integer.parseInt(p[2]);
			    	}
			    	else {  //e de tipul ax^b
			    		String s[]=myStrings[i].split("\\^|x");
			    		co=Double.parseDouble(s[0]);
			    		po=Integer.parseInt(s[2]);
			    	}
			    }
			    if (semne[d].equals("+")){
			    	monomR.add(new MonomReal(co,po,false));
			    }
			    else monomR.add(new MonomReal((-1)*co,po,false));
		   }
		  
		}
	}
	public static boolean isNumeric(String str)
	{
	  return str.matches("-?\\d+(\\.\\d+)?");  
	}
	public void setMonomR(MonomReal m1){
		monomR.add(m1);
	}
	public String toString(){
		return monomR.toString();
	}

	public String print(){
		String s="";
		for (MonomReal each:monomR){
			s=s+each.toString();
		}
		return s;
	}
}
