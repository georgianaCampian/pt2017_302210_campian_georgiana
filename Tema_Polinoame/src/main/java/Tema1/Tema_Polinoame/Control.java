package Tema1.Tema_Polinoame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class Control {
	private View v;
	Polinom polinom1,polinom2;
	public Control(View view) {
		v = view;
		view.addOKListener1(new OKListener1());
		view.addOKListener2(new OKListener2());
		view.addAdListener(new AdListener());
		view.addSListener(new SListener());
		view.addInmListener(new InmListener());
		view.addIntListener(new IntListener());
		view.addDerListener(new DerListener());
	}
	class OKListener1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			boolean ok=false;  //pt a determina semnul primului monom
			String p1 = "";
				p1 = v.getPol1();
				System.out.println(p1);
				String s=p1.replaceAll("-", "+");
				if (p1.substring(0,1).equals("-")){
					ok=true;
				}
				String[] myStrings1 = s.split("(\\+)");  //separam monoamele dintre semnnul +
				//constr semne va fi un string al semnelor, in ordinea coresp aparitiei fiecarui monom in polinom
				polinom1=new Polinom(myStrings1,constrSemne(myStrings1,ok,p1));  
		}
	}	
	class OKListener2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			boolean ok=false;
			String p2 = "";
			p2=v.getPol2();
			System.out.println(p2);
			String s=p2.replaceAll("-", "+");
			if (p2.substring(0,1).equals("-")){
				ok=true;
			}
			String[] myStrings2 = s.split("(\\+)");
			polinom2=new Polinom(myStrings2,constrSemne(myStrings2,ok,p2));
		}
	}	
	class AdListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Polinom sumaPol=polinom1.adunaPolinom(polinom2);
			System.out.println(sumaPol.print());
			v.setAdunare(sumaPol.print());
		}
	}	
	class SListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Polinom nPol=polinom1.minusPolinom(polinom2);
			Polinom difPol=polinom1.adunaPolinom(nPol);
			System.out.println(difPol.print());
			v.setScadere(difPol.print());
		}
	}	
	class InmListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Polinom iPol=polinom1.inmultirePolinom(polinom2);
			System.out.println(iPol.print());
			v.setInm(iPol.print());
		}
	}	
	class IntListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			PolinomReal iPol1=polinom1.integrarePolinom();
			PolinomReal iPol2=polinom2.integrarePolinom();
			System.out.println(iPol1.print());
			v.setIntegrat1(iPol1.print());
			System.out.println(iPol2.print());
			v.setIntegrat2(iPol2.print());
		}
	}	
	class DerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Polinom dPol1=polinom1.derivarePolinom();
			Polinom dPol2=polinom2.derivarePolinom();
			System.out.println(dPol1.print());
			v.setDerivat1(dPol1.print());
			System.out.println(dPol2.print());
			v.setDerivat2(dPol2.print());
		}
	}	
	private String[] constrSemne(String[] myStrings1,boolean ok,String pol1){
		String semne1[]={"+","+","+","+","+"};
		int i,t=0,parcPol=0,k=0;
		int nrMonoame1=myStrings1.length;
		if (ok==true) k=1;
		for (i=k; i<nrMonoame1; i++){  //pt fiecare monom vrem sa aflam semnul si il punem intr-un string
			int lgMonom=myStrings1[i].length();
			String c;  
			if (((i==1) && (ok==true)) || (i==0)){  //pt primul monom 
				c=pol1.substring(0,1);  
				if (c.equals("-")){
					semne1[t]="-";
					t++;
					lgMonom++;
				}
				else t++; 
				parcPol=parcPol+lgMonom; 
			}
			else {
				semne1[t]=pol1.substring(parcPol,parcPol+1);
				t++;
				parcPol=parcPol+lgMonom+1;
			}
		}
		return semne1;
	}

	
}